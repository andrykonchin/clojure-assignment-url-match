(ns clojure-assignment-url-match.core
  (:gen-class))

(require '[clojure.string])
(use '[clojure.java.io :only (as-url)])

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn new-pattern [patterns-string]
  (defn split-patterns [str]
    (map clojure.string/trim (clojure.string/split str #";")))

  (defn parse-pattern [str]
    (defn parse-host [s]
      [:host (get (re-matches #"host\(([^()]*)\)" s) 1)])

    (defn bind? [s] (= (clojure.core/str (get s 0)) "?"))

    (defn string-to-typed-value [s]
      (if (bind? s) [:bind (subs s 1)] [:string s]))

    (defn parse-path [s]
      (let [path (get (re-matches #"path\(([^()]*)\)" s) 1)
            segments (clojure.string/split path #"/")]
        [:path (map string-to-typed-value segments)]))

    (defn parse-queryparam [s]
      (let [key-value-pair (subvec (re-matches #"queryparam\(([^()]*)=([^()]*)\)" s) 1)
            param-name (get key-value-pair 0)
            param-value (get key-value-pair 1)]
        [:query [param-name (string-to-typed-value param-value)]]))

    (cond
      (clojure.string/starts-with? str "host") (parse-host str)
      (clojure.string/starts-with? str "path") (parse-path str)
      (clojure.string/starts-with? str "queryparam") (parse-queryparam str)
      :else (throw (Exception. "Unknown pattern name"))))

  (defn verify-patterns [patterns]
    (cond
      (>= (count (filter #(= (get % 0) :host) patterns)) 2) (throw (Exception. "Specified several host patterns"))
      (>= (count (filter #(= (get % 0) :path) patterns)) 2) (throw (Exception. "Specified several path patterns"))
      :else patterns))

  (let [patterns (split-patterns patterns-string)]
    (verify-patterns (map parse-pattern patterns))))

(defn recognize [patterns url-string]
  (defn parse-url [str]
    (defn parse-query [query-string]
      (into {} (map #(clojure.string/split % #"=") (clojure.string/split query-string #"&"))))
    (let [url (as-url str)
          query (if (.getQuery url) (parse-query (.getQuery url)) {})]
      {:host (.getHost url) :path (.getPath url) :query query}))

  (defn match-pattern [[pattern-type pattern] url]
    (defn match-host [expected-host, actual-url]
      (if (= expected-host (:host actual-url)) [] nil))

    (defn match-path [pattern-segments actual-url]
      (defn segment-match? [[segment, pattern-segment]]
        (let [pattern-segment-type (get pattern-segment 0)
              pattern-segment-value (get pattern-segment 1)]
          (if (= pattern-segment-type :string) (= segment pattern-segment-value) true)))

      (let [actual-path (:path actual-url)
            actual-segments (remove empty? (clojure.string/split actual-path #"/"))]
        (if (= (count pattern-segments) (count actual-segments))
          (if (every? segment-match? (map vector actual-segments pattern-segments))
            (map (fn [[s ps]] [(keyword (get ps 1)) s]) (filter (fn [[s, ps]] (= (get ps 0) :bind)) (map vector actual-segments pattern-segments)))
            [nil])
          [nil])))

    (defn match-query-parameter [[param-name [pattern-type pattern-value]] actual-url]
      (let [actual-param-value (get (:query actual-url) param-name)]
        (if (= pattern-type :string)
          (if (= actual-param-value pattern-value) [] nil)
          (if actual-param-value [(keyword pattern-value) actual-param-value] nil))))

    (cond
      (= pattern-type :host) [(match-host pattern url)]
      (= pattern-type :path) (match-path pattern url)
      (= pattern-type :query) [(match-query-parameter pattern url)]
      :else (throw (Exception. "Unknown pattern name"))))

  (defn successful-matching? [matching] (not (nil? matching)))
  (defn matching-without-binds? [matching] (empty? matching))

  (let [url (parse-url url-string)
        matchings (apply concat (map #(match-pattern % url) patterns))]
    (if (every? successful-matching? matchings) (remove matching-without-binds? matchings) nil)))
