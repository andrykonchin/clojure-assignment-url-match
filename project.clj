(defproject clojure-assignment-url-match "0.1.0-SNAPSHOT"
  :description "Assignment #1 match-url"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :main ^:skip-aot clojure-assignment-url-match.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
