(ns clojure-assignment-url-match.core-test
  (:require [clojure.test :refer :all]
            [clojure-assignment-url-match.core :refer :all]))

(deftest new-pattern-test
  (testing "host pattern"
    (is (= (new-pattern "host(twitter.com)")
           [[:host "twitter.com"]])))

  (testing "path pattern"
    (is (= (new-pattern "path(foo)")
           [[:path [[:string "foo"]]]]))

    (is (= (new-pattern "path(foo/bar)")
           [[:path [[:string "foo"] [:string "bar"]]]]))

    (is (= (new-pattern "path(?user/foo)")
           [[:path [[:bind "user"] [:string "foo"]]]]))

    (is (= (new-pattern "path(?user/foo/?id)")
           [[:path [[:bind "user"] [:string "foo"] [:bind "id"]]]])))

  (testing "queryparam pattern"
    (is (= (new-pattern "queryparam(filter=true)")
           [[:query ["filter" [:string "true"]]]]))

    (is (= (new-pattern "queryparam(offset=?value)")
           [[:query ["offset" [:bind "value"]]]]))

    (is (= (new-pattern "queryparam(offset=?value); queryparam(sort=true)")
           [[:query ["offset" [:bind "value"]]]
            [:query ["sort" [:string "true"]]]])))

  (testing "complex patterns"
    (is (= (new-pattern "host(twitter.com); path(?user/status/?id);")
           [[:host "twitter.com"]
            [:path [[:bind "user"] [:string "status"] [:bind "id"]]]]))

     (is (= (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);")
            [[:host "dribbble.com"]
             [:path [[:string "shots"] [:bind "id"]]]
             [:query ["offset" [:bind "offset"]]]]))

     (is (= (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")
            [[:host "dribbble.com"]
             [:path [[:string "shots"] [:bind "id"]]]
             [:query ["offset" [:bind "offset"]]]
             [:query ["list" [:bind "type"]]]])))

  (testing "parsing errors"
    (is (thrown-with-msg? Exception #"Specified several host patterns"
                 (doall (new-pattern "host(twitter.com); host(dribbble.com)"))))

    (is (thrown-with-msg? Exception #"Unknown pattern name"
                 (doall (new-pattern "foo(twitter.com)"))))

    (is (thrown-with-msg? Exception #"Specified several path patterns"
                 (new-pattern "path(stat/:id); path(stat/12)")))))

(deftest recognize-test
  (testing "matching without bindings"
    (is (= (recognize [[:host "google.com"]] "http://google.com/search?q=foo")
           []))

    (is (= (recognize [[:path [[:string "search"]]]] "http://google.com/search?q=foo")
           []))

    (is (= (recognize [[:query ["q" [:string "foo"]]]] "http://google.com/search?q=foo")
           []))

    (is (= (recognize [[:host "bing.com"]] "http://google.com/search?q=foo")
           nil))

    (is (= (recognize [[:path [[:string "foo"]]]] "http://google.com/search?q=foo")
           nil))

    (is (= (recognize [[:path [[:string "foo"] [:string "bar"]]]] "http://google.com/search?q=foo")
           nil))

    (is (= (recognize [[:query ["q" [:string "bar"]]]] "http://google.com/search?q=foo")
           nil))

    (is (= (recognize [[:query ["r" [:string "foo"]]]] "http://google.com/search?q=foo")
           nil)))

  (testing "matching with path binding"
    (is (= (recognize [[:path [[:bind "a"]]]] "http://google.com/search?q=foo")
           [[:a "search"]]))

    (is (= (recognize [[:path [[:bind "a"][:bind "b"]]]] "http://google.com/x/y/?q=foo")
           [[:a "x"] [:b "y"]]))

    (is (= (recognize [[:path [[:bind "a"][:string "y"][:bind "b"]]]] "http://google.com/x/y/z/?q=foo")
           [[:a "x"] [:b "z"]]))

    (is (= (recognize [[:query ["q" [:bind "a"]]]] "http://google.com/search?q=foo")
           [[:a "foo"]]))))

(deftest integration-tests
  (def twitter (new-pattern "host(twitter.com); path(?user/status/?id);"))
  (def dribbble (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"))

  (is (= (recognize twitter "http://twitter.com/bradfitz/status/562360748727611392")
         [[:user "bradfitz"] [:id "562360748727611392"]]))

  (is (= (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
         [[:id "1905065-Travel-Icons-pack"] [:offset "1"]]))

  (is (= (recognize dribbble "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
         nil))

  (is (= (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users")
         nil)))
